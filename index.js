var fs = require('fs');

const myArgs = process.argv.slice(2);
let filename = myArgs[0]
let file
try{
  file = JSON.parse(fs.readFileSync('./' + filename, 'utf8'))
}catch(e){
  throw new Error('Erreur à l ouverture')
  process.exit(1);
}

function index(newJson, name) {
  return Object.values(newJson).map((object) => {
    let tmpObject = object;
    if (!object.messages) {
      return object
    }
    tmpObject.messages = tmpObject.messages.map((message) => {
      message.engineId = name
      return message
    })

    return tmpObject
  })
}

fs.appendFile(myArgs[0], JSON.stringify(index(file, `Sonarviewer_${__dirname.split('/')[__dirname.split("/").length - 1]}`)),
  function (err) {
    if (err) {
      throw err;
      process.exit(1);
    }
    process.exit()
  });